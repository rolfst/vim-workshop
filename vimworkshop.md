        ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗    ██╗    ██╗ ██████╗ ██████╗ ██╗  ██╗███████╗██╗  ██╗ ██████╗ ██████╗ 
        ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║    ██║    ██║██╔═══██╗██╔══██╗██║ ██╔╝██╔════╝██║  ██║██╔═══██╗██╔══██╗
        ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║    ██║ █╗ ██║██║   ██║██████╔╝█████╔╝ ███████╗███████║██║   ██║██████╔╝
        ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║    ██║███╗██║██║   ██║██╔══██╗██╔═██╗ ╚════██║██╔══██║██║   ██║██╔═══╝ 
        ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║    ╚███╔███╔╝╚██████╔╝██║  ██║██║  ██╗███████║██║  ██║╚██████╔╝██║     
        ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝     ╚══╝╚══╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝     



## How do I start vim/neovim?
Or why do i have to?
  `sudo apt install neovim`
  `https://github.com/neovim/neovim/releases/download/v0.8.3/nvim-win64.msi`


## How do I close it?
It's not hard but you have to know modes.
  - normal mode
  - insert mode
  - visual mode
  - command mode


## Basic editing
Vim is made for editing not for writing.
  - motions
  - getting in insert mode
  - selections
  - searching
  - copy paste
  - increment/decrement
  - content from other files
  - buffers


## Vim settings
Things like:
  - line numbers
  - location of eg. swapfiles, undofiles
  - tabstops

### Using Lua cuz this is Neovim
Neovim is the next generation and has a real scripting language that has a broad community.
Lua is made for embedding.
Neovim binds vim api to lua and the other way around.


## Vim as an IDE/PDE
  - LSP
  - Dap
  - Treesitter
  - autocompletion


## Filetypes
  - vim automatically detects


##  Plugins
Handcrafted
  old school
Package manager
  newer school

